package Controlers;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import models.User;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

/**
 * Servlet implementation class registerCtrl
 */
@WebServlet("/registerCtrl")
public class insertUserCtrl extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * Default constructor. 
     */
    public insertUserCtrl() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String name = (String)request.getParameter("name");
		String surname = (String)request.getParameter("surname");
		String city = (String)request.getParameter("city");
		String phonenumber = (String)request.getParameter("phonenumber");
		String email = (String)request.getParameter("email");
	
		//MODEL
		JSONArray output = new JSONArray();
		JSONObject item = new JSONObject();
		
		if (User.insertUser(name, surname, city, phonenumber, email) !=0){
			item.put("status", "OK");
		}else {
			item.put("status", "KO");
		}
	}

	
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}
	
	

}
